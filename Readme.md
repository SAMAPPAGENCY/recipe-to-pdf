# Procédure d'installation et de configuration

Pré requis :
- Linux os
- Mettre à jour son linux :
	- sudo apt-get update
	- sudo apt-get upgrade
	- sudo apt install git
	- sudo apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common
- Installer docker et docker-compose
  - docker : https://docs.docker.com/engine/install/ubuntu/
  - docker-compose : https://docs.docker.com/compose/install/

### Tutoriel

1. **Cloner le dépôt git**

	```bash
	git clone https://github.com/EPradillon/poste-de-travail-java
	cd recipe-to-pdf
	```

2. **Construction des images docker**
	
	Pour déployer l'application, un `docker-compose.yml` a été configuré à la racine du projet.
	
	Il dispose de trois services :

	- app-server : configuration du backend (MVN, SPRING BOOT)
	- app-client : Configuraiton du front-end en React
	- db : base de données mysql

	Script d'exécution de la pile complète avec docker-compose (à exécuter à la racine du projet) :

	```bash
	docker-compose up
	```

	Commande pour arrêter tous les services à la fois à l'aide de docker compose
	```bash
	docker-compose down
	```

3. **Configuration du username et du password pour la connection à mysql**

	+ Ouvrir le fichier `src/main/resources/application.properties`.

	+ Changer `spring.datasource.username` et `spring.datasource.password`

## Visualisation de l'application

> L'adresse en local : localhost:9090
![App Screenshot](screenshot.png)


# Installation et utilisation de sonarqube

Dans votre terminal, tapez les 2 commandes suivantes :
```bash
docker pull sonarqube
docker run -d --name sonarqube -p 9000:9000 -p 9092:9092 sonarqube
```

Vous pouvez maintenant accéder au serveur SonarQube et consulter l’interface
sur http://localhost:9000. (ça peut durer quelques secondes avant de pouvoir accéder à
l’interface…)

Ensuite, il faut créer un nouveau projet sonar via l'interface et suivre les instructions pour connecter le projet à sonarqube.

#  Intégration continue

Une intégration continue a été mise en place et configuré dans le `.gitlab-ci.yml`.

Trois stages (étapes) ont été définis :
- build (excepté la branche master)
- test (excepté la branche master)
- deploy (seulement sur la branche master)

Afin de profiter de l'intégration continue, il est nécessaire de suivre un workflow type.

Exemple de workflow type :
Pour chaque tâche, il faut créer une branche à partir de la branche development. Une fois votre tâche terminée, vous faites un merge request de la branche courante vers la branche development. Si le job (pipeline) sur les tests passes, le merge sera validé. Une fois votre branche development testée et à jour, il faut faire un merge sur master, qui lancera le stage deploy pour déployer l'application.
